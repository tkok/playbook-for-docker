# playbook-for-docker
Ansible playbook to install docker and docker-compose.  
It expects to run on CentOS 8 or AlmaLinux 8  
because commands include dnf and systemctl.

## Usage
```
ansible-playbook -i inventory/hosts docker.yml

# with group, args
ansible-playbook -i inventory/hosts -l group_a docker.yml -e "initial=true"
```
